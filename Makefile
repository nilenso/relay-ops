# HELP sourced from https://gist.github.com/prwhite/8168133

# Add help text after each target name starting with '\#\#'
# A category can be added with @category

HELP_FUNC = \
    %help; \
    while(<>) { \
        if(/^([a-z0-9_-]+):.*\#\#(?:@(\w+))?\s(.*)$$/) { \
            push(@{$$help{$$2}}, [$$1, $$3]); \
        } \
    }; \
    print "usage: make [target]\n\n"; \
    for ( sort keys %help ) { \
        print "$$_:\n"; \
        printf("  %-30s %s\n", $$_->[0], $$_->[1]) for @{$$help{$$_}}; \
        print "\n"; \
    }

help:           ##@miscellaneous Show this help.
	@perl -e '$(HELP_FUNC)' $(MAKEFILE_LIST)


# REPORTS #############

clients: ##@reports Get list of clients [ENV]
	bin/clients

analytics: ##@reports Retrieve analytics for a client. [CLIENT_ID, ENV]
	bin/analytics

# LOCAL ############

codesign-mac: ##@local Codesign desktop build for mac.
	bin/codesign-mac

codesign-windows-installer: ##@local Codesign desktop build for windows.
	bin/codesign-windows-installer

generate-icons: ##@local Generate icons needed for desktop and mobile.
	bin/generate-icons

android-docker-build: ##@local Build docker image for building android app on CI
	docker build -t registry.gitlab.com/nilenso/relay-ops/relay-android .

android-docker-push:  ##@local Push docker image to gitlab registry for CI usage
	docker login registry.gitlab.com
	docker push registry.gitlab.com/nilenso/relay-ops/relay-android:latest

# BUILD ############

build-android: ##@build build android apk
	bin/build-android

build-ios: ##@build build iOS ipa
	bin/build-ios

# REBRAND ############

rebrand-desktop-app: ##@rebrand rebrand desktop app
	bin/rebrand-desktop-app

rebrand-mobile: ##@rebrand rebrand mobile apps
	bin/rebrand-mobile

rebrand-webapp: ##@rebrand rebrand webapp
	bin/rebrand-webapp

# SERVER ############

list-clients: ##@server get list of clients, ports, etc
	bin/list-clients

remove-service: ##@server clean up service of given client. does not delete data.
	bin/remove-service

reset-client-config: ##@server reset the client's config to defaults
	bin/reset-client-config

reset-db: ##@server reset the DB for a given client
	bin/reset-db

setup-giphy: ##@server one-time task to setup the giphy integration
	bin/setup-giphy

setup-new-mattermost: ##@server one-time task to setup mattermost service on a fresh instance
	bin/setup-new-mattermost

setup-shared-mattermost-app: ##@server add a new client
	bin/setup-shared-mattermost-app

upgrade-mattermost: ##@server upgrade mattermost to the newest version
	bin/upgrade-mattermost

create-relay-user: ##@server creates relay system admin user
	bin/create-relay-user

create-relaybot-user: ##@server creates a relay bot user and verifies it's email.
	EMAIL=relaybot@relay-chat.com USERNAME=relaybot PASSWORD=${BOT_PASSWORD} bin/create-relay-user
	BOT_EMAIL=relaybot@relay-chat.com BOT_USER=relaybot bin/verify-relaybot-user

delete-relay-user: ##@server deletes relay user
	bin/delete-relay-user

archive-mattermost-logs: ##@server moves older mattermost logs to s3
	bin/archive-mattermost-logs

setup-prometheus: ##@server sets up prometheus on instance
	bin/setup-prometheus

setup-grafana: ##@server sets up grafana on instance
	bin/setup-grafana

setup-node-exporter: ##@server sets up node exporter on instance
	bin/setup-node-exporter

setup-process-exporter: ##@server sets up process exporter on instance
	bin/setup-process-exporter

update-prometheus-config: ##@server updates config for prometheus
	bin/update-prometheus-config

# RELAY BACKEND ######

setup-relay-backend-server: ##@relay_backend one-time task to setup fresh instance for relay-backend
	bin/setup-relay-backend-server

deploy-relay-backend: ##@relay_backend one-time task to setup fresh instance for relay-backend
	bin/deploy-relay-backend
