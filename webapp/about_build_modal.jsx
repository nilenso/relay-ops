// Copyright (c) 2015-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

import PropTypes from 'prop-types';
import React from 'react';
import {Modal} from 'react-bootstrap';
import {FormattedHTMLMessage, FormattedMessage} from 'react-intl';

import RelayRedLogo from 'components/svg/relay_red_logo';

export default class AboutBuildModal extends React.PureComponent {
    static defaultProps = {
        show: false
    };

    static propTypes = {

        /**
         * Determines whether modal is shown or not
         */
        show: PropTypes.bool.isRequired,

        /**
         * Function that is called when the modal is dismissed
         */
        onModalDismissed: PropTypes.func.isRequired,

        /**
         * Global config object
         */
        config: PropTypes.object.isRequired,

        /**
         * Global license object
         */
        license: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.doHide = this.doHide.bind(this);
    }

    doHide() {
        this.props.onModalDismissed();
    }

    render() {
        const config = this.props.config;
        const license = this.props.license;

        let learnMore = (
            <div>
                <FormattedMessage
                    id='join-relay-community'
                    defaultMessage='Join the relay community at '
                />
                <a
                    target='_blank'
                    rel='noopener noreferrer'
                    href='https://open.relay-chat.com/'
                >
                    {'open.relay-chat.com'}
                </a>
            </div>
        );

        return (
            <Modal
                dialogClassName='about-modal'
                show={this.props.show}
                onHide={this.doHide}
            >
                <Modal.Header closeButton={true}>
                    <Modal.Title>{'About Relay'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='about-modal__content'>
                        <div className='about-modal__logo'>
                            <RelayRedLogo/>
                        </div>
                        <div>
                            <h3 className='about-modal__title'>{'Relay'}</h3>
                            <p className='about-modal__subtitle padding-bottom'>{'Workplace Messaging. Hosted. Affordable. Open Source.'}</p>
                        </div>
                    </div>
                    <div className='about-modal__footer'>
                        {learnMore}
                        <div className='form-group about-modal__copyright'>
                            <FormattedMessage
                                id='about-mattermost-copyright'
                                defaultMessage='Copyright 2015 - {currentYear} Mattermost, Inc. All rights reserved'
                                values={{
                                    currentYear: new Date().getFullYear()
                                }}
                            />
                        </div>
                    </div>
                    <div className='about-modal__notice form-group padding-top x2'>
                        <p>
                            <FormattedHTMLMessage
                                id='about-relay-notice'
                                defaultMessage='Relay is made possible by the open source software used in our <a href="https://about.mattermost.com/platform-notice-txt/" target="_blank">platform</a>, <a href="https://about.mattermost.com/desktop-notice-txt/" target="_blank">desktop</a> and <a href="https://about.mattermost.com/mobile-notice-txt/" target="_blank">mobile</a> apps.'
                            />
                        </p>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}
