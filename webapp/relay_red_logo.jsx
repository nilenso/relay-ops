// Copyright (c) 2017-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

import React from 'react';

export default class RelayRedLogo extends React.PureComponent {
    render() {
        return (
            <span {...this.props}>
                <img width="79px" src="https://s3.ap-south-1.amazonaws.com/objects.amongdragons/release/assets/logo.png" alt="Relay" />
            </span>
        );
    }
}

const style = {
    background: {
        enableBackground: 'new 0 0 500 500'
    },
    st0: {
        fillRule: 'evenodd',
        clipRule: 'evenodd'
    }
};
