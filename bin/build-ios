#!/bin/sh

set -ex

ROOT_DIR=$(cd $(dirname $0)/..; pwd)
WORK_DIR=${ROOT_DIR}/..

if [ -z ${IOS_RELEASE} ]; then
    echo "Please try again with IOS_RELEASE set."
    exit 1;
fi

. ${ROOT_DIR}/bin/build-mobile

### Install deps

cd ${MM_MOBILE_DIR}/ios
pod install
# TODO: setup cocoapads: https://stackoverflow.com/a/25257238

### Update ios Info.list

sed -i '5 i\ <key>UIRequiresFullScreen</key>' ${MM_MOBILE_DIR}/ios/Mattermost/Info.plist
sed -i '6 i\ <true/>' ${MM_MOBILE_DIR}/ios/Mattermost/Info.plist

### Update icons file
sed -i 's/icon-60@2x.png/Icon-60@2x.png/g' \
    ${MM_MOBILE_DIR}/ios/Mattermost/Images.xcassets/AppIcon.appiconset/Contents.json

sed -i 's/icon-60@3x.png/Icon-60@3x.png/g' \
    ${MM_MOBILE_DIR}/ios/Mattermost/Images.xcassets/AppIcon.appiconset/Contents.json

sed -i 's/icon-72@2x.png/Icon-72@2x.png/g' \
    ${MM_MOBILE_DIR}/ios/Mattermost/Images.xcassets/AppIcon.appiconset/Contents.json

### Build

\cp ${ROOT_DIR}/templates/bundleReactNative.sh ${MM_MOBILE_DIR}/ios/bundleReactNative.sh
cd ${MM_MOBILE_DIR}
react-native bundle --dev false --assets-dest ./ios --entry-file index.js --platform ios --bundle-output ios/main.jsbundle
cd ${MM_MOBILE_DIR}/ios
xcproj -t Mattermost add-resources-bundle main.jsbundle
xcproj -t Mattermost add-resources-bundle assets
cd ${MM_MOBILE_DIR}
make build-ios ${IOS_RELEASE}
