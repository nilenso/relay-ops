SELECT d.datname
FROM pg_database d
INNER JOIN pg_user u ON d.datdba = u.usesysid
WHERE u.usename != 'rdsadmin'
AND d.datname not like 'template%'
AND d.datname != 'postgres';
