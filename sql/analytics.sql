PREPARE audit_log_info as
SELECT to_timestamp(a.createat/1000) as createat, u.username, a.action, a.extrainfo, a.ipaddress, a.sessionid
FROM audits a
LEFT JOIN users u on a.userid=u.id
ORDER BY createat desc
LIMIT 20;

PREPARE sessions_info AS
SELECT to_timestamp(s.createat/1000) as createat, to_timestamp(s.lastactivityat/1000) as lastactivityat, u.username, s.props
FROM sessions s
INNER JOIN users u on s.userid = u.id
ORDER BY createat;

PREPARE channels_info AS
SELECT to_timestamp(c.createat/1000) as createat, t.name as team, c.type, c.displayname, to_timestamp(c.lastpostat/1000) as lastpostat, count(p.id) as post_count
FROM channels c
INNER JOIN posts p on c.id = p.channelid
INNER JOIN teams t on t.id = c.teamid
GROUP BY c.id, t.id
ORDER BY createat;

PREPARE posts_info_user_channel AS
SELECT EXTRACT(WEEK FROM to_timestamp(p.createat/1000)) as week,
       c.displayname as channel, u.username, count(u.username)
FROM posts p
INNER JOIN users u on p.userid=u.id
INNER JOIN channels c on c.id=p.channelid
GROUP BY u.username, channel, week
ORDER BY week, channel;

PREPARE posts_info AS
SELECT EXTRACT(WEEK FROM to_timestamp(p.createat/1000)) as week,
       count(u.username) as post_count
FROM posts p
INNER JOIN users u on p.userid=u.id
GROUP BY week
ORDER BY week;

PREPARE teams_info AS
SELECT to_timestamp(createat/1000) as createat, displayname, email
FROM teams
ORDER BY createat;

PREPARE users_info AS
SELECT to_timestamp(createat/1000), username, email, roles, emailverified
FROM users
ORDER BY createat;

\echo '--------------------- TEAMS_INFO ---------------------\n'
EXECUTE teams_info;
\echo '--------------------- CHANNELS_INFO ---------------------\n'
EXECUTE channels_info;
\echo '--------------------- AUDIT_LOG_INFO ---------------------\n'
EXECUTE audit_log_info;
\echo '--------------------- USERS_INFO ---------------------\n'
EXECUTE users_info;
\echo '--------------------- POSTS_BY_USER_AND_CHANNEL ---------------------\n'
EXECUTE posts_info_user_channel;
\echo '--------------------- POSTS_BY_WEEK ---------------------\n'
EXECUTE posts_info;

\echo '--------------------- SESSIONS_INFO ---------------------\n'
EXECUTE sessions_info;
