# Mattermost => Relay build and ops

This repository is suite of ops related scripts for hosting, and re-labeling mattermost.

## Usage

Run `make help`, to see the available functionalities. Each target would prompt you to use necessary ENV variables.
```
usage: make [target]

build:
  build-android                  build android apk
  build-ios                      build iOS ipa

local:
  codesign-mac                   Codesign desktop build for mac.
  codesign-windows-installer     Codesign desktop build for windows.
  generate-icons                 Generate icons needed for desktop and mobile.

miscellaneous:
  help                           Show this help.

rebrand:
  rebrand-desktop-app            rebrand desktop app
  rebrand-mobile                 rebrand mobile apps
  rebrand-webapp                 rebrand webapp

reports:
  clients                        Get list of clients [ENV]
  analytics                      Retrieve analytics for a client. [CLIENT_ID, ENV]

server:
  list-clients                   get list of clients, ports, etc
  remove-service                 clean up service of given client. does not delete data.
  reset-client-config            reset the client's config to defaults
  reset-db                       reset the DB for a given client
  setup-giphy                    one-time task to setup the giphy integration
  setup-new-mattermost           one-time task to setup mattermost service on a fresh instance
  setup-shared-mattermost-app    add a new client
  upgrade-mattermost             upgrade mattermost to the newest version
```

## Dev setup

```
# needed for rebranding:
brew install gnu-sed

# needed for building mattermost-webapp:
brew install node
npm install -g yarn

# needed for running locally:
brew install docker
```

## Running mattermost locally for development
```
# Start webserver and client
# This start the required serivces as docker containers, # webpack-dev-server for webapp,
# and runs mattermost server locally on :8065
# Now, any changes made to the webapp should auto-reload in the browser
cd $GOPATH/src/github.com/mattermost/mattermost-server
make run

# Stop running services
make stop
```

## Deploying latest mattermost-server on relay
- Download rebranded mattermost-server build from gitlab-ci
- scp build to the server to be upgraded, then ssh into the machine and copy build to /opt
- Finally restart the services.
```
scp -i $SECRETS_FILE $LOCAL_PATH_TO_MATTERMOST $DEPLOY_USER@$DEPLOY_SERVER:~
mv /bin/mattermost /bin/mattermost$DATE.bkp
cp -r ~/mattermost /bin/mattermost
sudo service mattermost-* restart
```
