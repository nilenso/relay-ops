// Copyright (c) 2017-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Svg from 'react-native-svg';

export default class AwayStatus extends PureComponent {
    static propTypes = {
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    };

    render() {
        return (
            <Svg
                height={this.props.height}
                width={this.props.width}
                viewBox='0 0 500 500'
            />
        );
    }
}
