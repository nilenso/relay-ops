// Copyright (c) 2017-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {
    Linking,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import DeviceInfo from 'react-native-device-info';

import FormattedText from 'app/components/formatted_text';
import StatusBar from 'app/components/status_bar';
import {changeOpacity, makeStyleSheetFromTheme, setNavigatorStyles} from 'app/utils/theme';

import AppIcon from 'app/components/app_icon';
import Config from 'assets/config';

const MATTERMOST_BUNDLE_IDS = ['com.mattermost.rnbeta', 'com.mattermost.rn'];

export default class About extends PureComponent {
    static propTypes = {
        config: PropTypes.object.isRequired,
        license: PropTypes.object.isRequired,
        navigator: PropTypes.object.isRequired,
        theme: PropTypes.object.isRequired
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.theme !== nextProps.theme) {
            setNavigatorStyles(this.props.navigator, nextProps.theme);
        }
    }

    handleAboutTeam = () => {
        Linking.openURL('https://open.relay-chat.com');
    };

    handleAboutEnterprise = () => {
        Linking.openURL(Config.AboutEnterpriseURL);
    };

    handlePlatformNotice = () => {
        Linking.openURL(Config.PlatformNoticeURL);
    };

    handleMobileNotice = () => {
        Linking.openURL(Config.MobileNoticeURL);
    };

    render() {
        const {theme, config, license} = this.props;
        const style = getStyleSheet(theme);

        let subTitle = (
            <FormattedText
                id='about-teamEditionSt'
                defaultMessage='Workplace Messaging. Hosted. Affordable. Open Source.'
                style={style.subtitle}
            />
        );

        let learnMore = (
            <View style={style.learnContainer}>
                <FormattedText
                    id='join-relay-community'
                    defaultMessage='Join the relay community at '
                    style={style.learn}
                />
                <TouchableOpacity
                    onPress={this.handleAboutTeam}
                >
                    <Text style={style.learnLink}>
                        {'open.relay-chat.com'}
                    </Text>
                </TouchableOpacity>
            </View>
        );

        return (
            <View style={style.wrapper}>
                <StatusBar/>
                <ScrollView
                    style={style.scrollView}
                    contentContainerStyle={style.scrollViewContent}
                >
                    <View style={style.logoContainer}>
                        <AppIcon
                            color={theme.centerChannelColor}
                            height={120}
                            width={120}
                        />
                    </View>
                    <View style={style.infoContainer}>
                        <View style={style.titleContainer}>
                            <Text style={style.title}>
                                {'Relay'}
                            </Text>
                        </View>
                        {subTitle}
                        {learnMore}
                        {!MATTERMOST_BUNDLE_IDS.includes(DeviceInfo.getBundleId()) &&
                            <FormattedText
                                id='mobile.about.powered_by'
                                defaultMessage='{site} is powered by Mattermost'
                                style={style.footerText}
                                values={{
                                    site: this.props.config.SiteName
                                }}
                            />
                        }
                        <FormattedText
                            id='mobile-about-copyright'
                            defaultMessage='Copyright 2015-{currentYear} Mattermost, Inc. All rights reserved'
                            style={style.footerText}
                            values={{
                                currentYear: new Date().getFullYear()
                            }}
                        />
                        <View style={style.noticeContainer}>
                            <View style={style.footerGroup}>
                                <FormattedText
                                    id='mobile.notice_text'
                                    defaultMessage='Mattermost is made possible by the open source software used in our {platform} and {mobile}.'
                                    style={style.footerText}
                                    values={{
                                        platform: (
                                            <FormattedText
                                                id='mobile.notice_platform_link'
                                                defaultMessage='platform'
                                                style={style.noticeLink}
                                                onPress={this.handlePlatformNotice}
                                            />
                                        ),
                                        mobile: (
                                            <FormattedText
                                                id='mobile.notice_mobile_link'
                                                defaultMessage='mobile apps'
                                                style={[style.noticeLink, {marginLeft: 5}]}
                                                onPress={this.handleMobileNotice}
                                            />
                                        )
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const getStyleSheet = makeStyleSheetFromTheme((theme) => {
    return {
        wrapper: {
            flex: 1,
            backgroundColor: theme.centerChannelBg
        },
        scrollView: {
            flex: 1,
            backgroundColor: changeOpacity(theme.centerChannelColor, 0.06)
        },
        scrollViewContent: {
            paddingBottom: 30
        },
        logoContainer: {
            alignItems: 'center',
            flex: 1,
            height: 200,
            paddingVertical: 40
        },
        infoContainer: {
            flex: 1,
            flexDirection: 'column',
            paddingHorizontal: 20
        },
        titleContainer: {
            flex: 1,
            flexDirection: 'row',
            marginBottom: 20
        },
        title: {
            fontSize: 22,
            color: theme.centerChannelColor
        },
        subtitle: {
            color: changeOpacity(theme.centerChannelColor, 0.5),
            fontSize: 19,
            marginBottom: 15
        },
        learnContainer: {
            flex: 1,
            flexDirection: 'column',
            marginVertical: 20
        },
        learn: {
            color: theme.centerChannelColor,
            fontSize: 16
        },
        learnLink: {
            color: theme.linkColor,
            fontSize: 16
        },
        info: {
            color: theme.centerChannelColor,
            fontSize: 16,
            lineHeight: 19
        },
        licenseContainer: {
            flex: 1,
            flexDirection: 'row',
            marginTop: 20
        },
        noticeContainer: {
            flex: 1,
            flexDirection: 'column'
        },
        noticeLink: {
            color: theme.linkColor,
            fontSize: 11,
            lineHeight: 13
        },
        hashContainer: {
            flex: 1,
            flexDirection: 'column'
        },
        footerGroup: {
            flex: 1,
            flexDirection: 'row'
        },
        footerText: {
            color: changeOpacity(theme.centerChannelColor, 0.5),
            fontSize: 11,
            lineHeight: 13,
            marginBottom: 10
        }
    };
});
