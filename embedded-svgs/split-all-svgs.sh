#!/bin/sh

split -l 1 all-svgs zig

for f in zig*; do
         mv "$f" "$f.svg"
done
